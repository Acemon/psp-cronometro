import javax.swing.*;

/**
 * Created by Fernando on 15/01/2017.
 */
public class Tempo extends SwingWorker <String, Void> {
    int tiempo;
    boolean parar;
    boolean pausar;

    public Tempo(int tiempo) {
        this.tiempo = tiempo;
        this.parar=false;
        this.pausar=false;
    }

    @Override
    protected String doInBackground() throws Exception {
        int cont=0;
        while (cont<tiempo){
            Thread.sleep(1);
            cont++;
            firePropertyChange("Tiempo", 0, cont);
            while (pausar){
                Thread.sleep(500);
            }
            if (parar){
                firePropertyChange("Fin", 0, "Fin");
                return null;
            }
        }
        firePropertyChange("Fin", 0, "Fin");
        parar = true;
        return null;
    }

    public void setParar(boolean parar) {
        this.parar = parar;
    }

    public boolean isPausar() {
        return pausar;
    }

    public void setPausar(boolean pausar) {
        this.pausar = pausar;
    }
}
