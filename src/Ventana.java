import javax.swing.*;

/**
 * Created by Fernando on 15/01/2017.
 */
public class Ventana {
    JTextField tfCantidad;
    JButton btLanzar;
    JButton btDetener;
    JButton btMostrar;
    JProgressBar pbProgreso;
    JLabel lmensaje;
    JLabel ltiempo;
    JPanel panel1;

    public Ventana() {
        JFrame frame = new JFrame("Ventana");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
