import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * Created by Fernando on 15/01/2017.
 */
public class Controller implements ActionListener {
    Ventana view;
    Tempo tempo;
    public Controller(Ventana view) {
        this.view=view;
        tempo = null;
        view.btLanzar.addActionListener(this);
        view.btLanzar.setActionCommand("Iniciar");
        view.btDetener.addActionListener(this);
        view.btDetener.setActionCommand("Detener");
        view.btMostrar.addActionListener(this);
        view.btMostrar.setActionCommand("Mostrar");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equalsIgnoreCase("Iniciar")){
            if (tempo == null){
                inicio();
            } if (tempo.isPausar()){
                tempo.setPausar(false);
            } if (tempo.parar){
                inicio();
            }

        }else if(e.getActionCommand().equalsIgnoreCase("Detener")){
            if (!tempo.isPausar()){
                tempo.setPausar(true);
            }

        }else if (e.getActionCommand().equalsIgnoreCase("Mostrar")){
            tempo.setParar(true);
            view.lmensaje.setText("Cuenta Finalizada");
            view.pbProgreso.setValue(view.pbProgreso.getMaximum());
            view.ltiempo.setText(String.valueOf(view.pbProgreso.getMaximum()));
        }
    }

    private void inicio() {
        view.pbProgreso.setMaximum(Integer.parseInt(view.tfCantidad.getText()));
        view.lmensaje.setText("");
        view.ltiempo.setText("0 msg");
        tempo = new Tempo(Integer.parseInt(view.tfCantidad.getText()));
        tempo.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (evt.getPropertyName().equalsIgnoreCase("tiempo")){
                    view.ltiempo.setText(evt.getNewValue().toString()+" msg");
                    view.pbProgreso.setValue((Integer) evt.getNewValue());
                }if (evt.getPropertyName().equalsIgnoreCase("Fin")){
                    view.lmensaje.setText("Cuenta Finalizada");
                }
            }
        });
        tempo.execute();
    }
}
